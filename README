NAME
        echobible - The Echo Bible Reader 

SYNOPSIS
        echobible

DESCRIPTION
        The Echo Bible Reader (echobible) is a bible reader written in shell 
        script (GNU Bash) for the command line interface; echobible writes the 
        text of the bible to stdout verse by verse (or by multiple verses at a 
        time). The functionality is interactively driven by hotkeys. For example, 
        the down arrow key writes the next verse to stdout. However, more useful
        methods of accessing and manipulating the text have been implemented.

        The text used is the King James Bible: Pure Cambridge Edition, however
        any bible can be formatted to work with the echobible. I plan on adding
        more bibles soon. Bibles must reside in the $HOME/echobible directory.

INSTALL AND UNINSTALL
        Use existing Makefile.

        See the accompanying INSTALL file.

FEATURES
        Concordance
        Dictionary
        Annotation
        Bookmarks
        Quickmarks
        Highlight
        Underline
        Go to any verse/chapter/book quickly, with a single command
        Text-To-Speech
        Autoverse (automatically advance verses)
        Search (words or phrases)
        Print verse(s)/chapter(s)/book(s)/bible to less/file/printer

HOW TO FORMAT BIBLES
        Documentation on this matter will be made soon.

CONFIGURATION
        A settings file exists as $HOME/echobible/settings

        The variables specified in the settings file are:

        BIBLE           The bible currently being used.

        DICT            The dict dictionary currently being used.

        FOLD_WIDTH      The width at which echobible will "fold" lines. Default
                        is 80.

        JUMP_COUNT      Amount of verses to "jump" or write to stdout at a
                        time. Default is 7 verses.

        WELCOME         Show the welcome screen. Either true or false.

        SPEAK           Perform text to speech. Dependency "festival" must be
                        satisfied.

        TITLE_XTERM     Show the chapter in the title of an xterm window.
                        Either true or false.

        FOLD            Fold lines so that the line does not too long. Either 
                        true or false.

        POSITION_FORMAT For the position indicator at the beginning of each 
                        verse. The three formats are:

                        value   literal                example
                        ---------------------------------------
                        bcv     BOOK CHAPTER:VERSE     GE 10:10
                        cv      CHAPTER:VERSE          10:10
                        v       VERSE                  10


        MAX_SCREEN_LINES    The maximum number of lines that echobible can
                            write on the screen before it is cleared; when the
                            screen is cleared, the subsequent verse be written
                            at the top of the screen. This functionality was 
                            implemented so that the user is not always looking 
                            at the very bottom of the terminal, which is where 
                            the verses would eventually output if the screen
                            was not cleared.

        CLEAR_SCREEN        Clear the screen once MAX_SCREEN_LINES is reached.
                            Either true or false.

        HIGHLIGHT_COLOR     This "setting" should not be here. It is an
                            internal value that only keeps track of
                            the current highlight color. The user should
                            not have access or need of using this value because
                            the highlight color can be selected interactively
                            from a menu by pression the 'H' key.

        ALWAYS_PIPE_TO_LESS     After a less session is exited, prompt the user
                                to pipe the next chapter. Either true
                                or false.

        AUTO_VERSE_TIME_MULTIPLIER      Number of seconds to wait before the
                                        next verse is written. Default is 2.5
                                        seconds.

HOTKEYS
        VERSE MOVEMENT:
        j, up arrow, spacebar   next verse
        k, down arrow           previous verse
        J, page down            next (7) verses
        K, page up              previous (7) verses
        >                       auto next verse
        <                       auto previous verse

        CHAPTER MOVEMENT:
        n, left arrow, ]        next chapter
        N, right arrow, [       previous chapter

        BOOK MOVEMENT:
        {                       next book
        }                       previous book

        BIBLE:
        B                       bible select

        BOOK:
        b                       book select

        GO:
        g                       go to book/chapter/verse
        G                       go select

        ANNOTATION:
        a                       annotate verse
        A                       delete annotation (menu)

        UNDERLINE:
        u                       toggle underline on verse

        HIGHLIGHT:
        h                       toggle highlight
        H                       highlight color select

        BOOKMARK:
        M                       bookmark verse
        R                       bookmark menu

        MARK:
        m                       mark verse
        r                       recall marked verse

        SCREEN:
        C                       toggle clear screen feature
                                (see MAX_SCREEN_LINES in settings)

        PRINT:
        p                       print select
        v                       print verse(s)
        l                       print current chapter (pipes to less)
        e                       echo current verse
        L                       toggle "always pipe to less"

        FIND:
        f, /                    find a string of text
        F                       toggle fold lines

        SPEECH:
        s                       speak verse
        S                       toggle Text-To-Speech

        CONCORDANCE:
        c                       concordance

        DICTIONARY:
        d                       dictionary
        D                       select dict dictionary

        MISCELLANEOUS:
        q                       quit

        HELP:
        F1, ?                   help 

BIBLES
        kjv-pce - King James Bible: Pure Cambridge Edition (bibleprotector.com)

DICTIONARIES
        Any dict dictionary is useable. However, the following dictionaries are 
        currently enabled to work:

        easton
        hitchcock 
        gcide 
        wn 
        moby-the

CONCORDANCES
        Concordances must exist in the echobible directory, and it must be
        prefixed with the filename of the bible, a hypen, and "concordance":

        (bible-filename)-concordance
        
        For example:

        $HOME/echobible/kjv-pce-concordance

BUGS
        Underline doesn't apply fully to a verse w/red words when the red is 
        escaped mid-sentence.

        Sometimes when folding concordance results, an blank line will appear 
        in the results. It seems to be a fold bug.

        PrintVerse will prompt user about using less regardless of whether the 
        verses specified are within range.

LICENSE
        echobible is distributed under the terms of the GNU General Public 
        License 3. See the accompanying COPYING file.

SEE ALSO
        awk (1), bible-kjv

AUTHOR
        James Polanco <jpolcol@gmail.com>
