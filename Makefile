# Written by James Polanco

#Makefile for echobible
#Install: sudo make install
#Uninstall: sudo make uninstall

install:
	@echo "Installing echobible..."
	install -Dm755 echobible "/usr/bin/echobible"
	install -d "/usr/share/licenses/echobible"
	install -Dm644 COPYING "/usr/share/licenses/echobible"
	install -d "/usr/share/echobible"
	install -Dm644 README "/usr/share/echobible"
	install -Dm644 TODO "/usr/share/echobible"
	install -Dm644 INSTALL "/usr/share/echobible"
	install -Dm644 AUTHORS "/usr/share/echobible"
	install -Dm644 CHANGELOG "/usr/share/echobible"
	install -Dm644 kjv-pce "/usr/share/echobible"
	install -Dm644 kjv-pce-concordance "/usr/share/echobible"

uninstall:
	@echo "Uninstalling echobible..."
	rm -f "/usr/bin/echobible"
	rm -f "/usr/share/licenses/echobible/COPYING"
	rm -R "/usr/share/licenses/echobible"
	rm -f "/usr/share/echobible/COPYING"
	rm -f "/usr/share/echobible/README"
	rm -f "/usr/share/echobible/TODO"
	rm -f "/usr/share/echobible/INSTALL"
	rm -f "/usr/share/echobible/AUTHORS"
	rm -f "/usr/share/echobible/CHANGELOG"
	rm -f "/usr/share/echobible/kjv-pce"
	rm -f "/usr/share/echobible/kjv-pce-concordance"
	rm -R "/usr/share/echobible"

.PHONY: install uninstall
